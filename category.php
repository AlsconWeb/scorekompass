<?php
/**
 * The template for displaying Category pages
 *
 * @link       https://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header(); ?>

	<div id="main" class="site-main">
		<div id="main-content" class="main-content">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					<div class="page-title">
						<div class="center">
							<h1><span>Score Kompass blog</span> — Ihr persönlicher Finanzratgeber</h1>
						</div>
					</div>
					<div class="center">
						<div class="news-l">


							<?php
							if ( have_posts() ) :
								// Start the Loop.
								while ( have_posts() ) :
									the_post();

									/*
									 * Include the post format-specific template for the content. If you want to
									 * use this in a child theme, then include a file called called content-___.php
									 * (where ___ is the post format) and that will be used instead.
									 */
									get_template_part( 'content', get_post_format() );

								endwhile;
								// Previous/next post navigation.
								twentyfourteen_paging_nav();

							else :
								// If no content, include the "No posts found" template.
								get_template_part( 'content', 'none' );

							endif;
							?>
						</div>
						<div class="news-r">
							<?php echo wp_kses_post( get_sidebar( 'blog' ) ); ?>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
