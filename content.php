<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
?>

<article class="news-list">
	<?php
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
	?>
	<div class="news-list">
		<div class="entry-meta">
			<span class="entry-date"><a rel="" href="#">
					<time datetime="" class="entry-date">
						<?php echo esc_html( get_the_date( 'j. F Y', $post->ID ) ); ?>
					</time>
				</a>
			</span>
		</div>
		<header class="entry-header">
			<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
				<div class="entry-meta">
					<span class="cat-links"><?php echo wp_kses_post( get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ) ); ?></span>
				</div>
			<?php
			endif;

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			?>
		</header><!-- .entry-header -->
		<a class="post-thumbnail" aria-hidden="true" href="<?php echo esc_url( get_permalink( $post->ID ) ); ?> ">
			<?php
			if ( ! empty( $feat_image ) ) {
				echo '<img src=' . esc_url( $feat_image ) . ' alt="Image">';
			}
			?>
		</a>
		<?php if ( is_search() ) : ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
			<?php
			the_excerpt();

			wp_link_pages(
				[
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				]
			);
			?>
			<a class="read-more" href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>">Weiterlesen</a>
		</div><!-- .entry-content -->
	</div>
<?php endif; ?>

	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->
