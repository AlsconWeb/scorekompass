<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="site-info">
		<div class="top-foot">
			<div class="center">
				<span class="call">Kostenlose Hotline: 0800 – 0 675 675</span>
				<?php
				$defaults = [
					'theme_location'  => 'footer',
					'menu'            => 'footer menu',
					'container'       => false,
					'container_class' => false,
					'container_id'    => false,
					'menu_class'      => 'mobile_menu',
					'menu_id'         => 'mobile_menu',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				];
				wp_nav_menu( $defaults );
				?>

			</div>
		</div>
		<div class="bott-foot">
			<div class="center">
				<p>&copy; 2014 - <?php echo esc_html( gmdate( 'Y' ) ); ?> Score Kompass</p>
			</div>
		</div>
	</div>
</footer>
</div>


<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		var body = $( 'body' ),
			_window = $( window ),
			nav, button, menu;

		nav = $( '#primary-navigation' );
		button = nav.find( '.menu-toggle' );
		menu = nav.find( '.nav-menu' );

		// Enable menu toggle for small screens.
		( function() {
			if ( ! nav || ! button ) {
				return;
			}

			// Hide button if menu is missing or empty.
			if ( ! menu || ! menu.children().length ) {
				button.hide();
				return;
			}

			button.on( 'click.twentyfourteen', function() {
				nav.toggleClass( 'toggled-on' );
				if ( nav.hasClass( 'toggled-on' ) ) {
					$( this ).attr( 'aria-expanded', 'true' );
					menu.attr( 'aria-expanded', 'true' );
				} else {
					$( this ).attr( 'aria-expanded', 'false' );
					menu.attr( 'aria-expanded', 'false' );
				}
			} );
		} )();
	} );
</script>

<?php wp_footer(); ?>
</body>
</html>