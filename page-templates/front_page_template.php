<?php
/**
 * Template Name: Home Page Template
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header(); ?>

	<div id="main" class="site-main">
		<div id="main-content" class="main-content">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
					<div class="page-title">
						<div class="center">
							<h2><span>Score Kompass blog</span> — Ihr persönlicher Finanzratgeber</h2>
						</div>
					</div>
					<div class="center">
						<div class="news-l">
							<article class="news-list">
								<div class="entry-meta">
									<span class="entry-date"><a rel="" href="#"><time datetime="" class="entry-date">2. Juni 2015</time></a></span>
								</div>
								<header class="entry-header">
									<h1 class="entry-title"><a rel="bookmark" href="#">Mietratgeber</a></h1>
								</header>
								<a class="post-thumbnail" aria-hidden="true" href="">
									<img src="images/post-img1.png" alt=""/>
								</a>
								<div class="entry-content">
									<p>Ein Sportwagen, eine Weltreise, ein Traumhaus an der Küste oder ganz
										bodenständig: die Altersvorsorge. Wir alle haben Ziele und Träume, für die es
										sich zu sparen lohnen. <a href="#">Weiterlesen</a></p>
								</div>
							</article>

							<article class="news-list">
								<div class="entry-meta">
									<span class="entry-date"><a rel="" href="#"><time datetime="" class="entry-date">2. Juni 2015</time></a></span>
								</div>
								<header class="entry-header">
									<h1 class="entry-title"><a rel="bookmark" href="#">Sparziele erreichen - mit diesen
											5 Tipps klappt es sicher</a></h1>
								</header>
								<div class="entry-content">
									<p>Ein Sportwagen, eine Weltreise, ein Traumhaus an der Küste oder ganz
										bodenständig: die Altersvorsorge. Wir alle haben Ziele und Träume, für die es
										sich zu sparen lohnt <a href="#">Weiterlesen</a></p>
								</div>
							</article>

							<article class="news-list">
								<div class="entry-meta">
									<span class="entry-date"><a rel="" href="#"><time datetime="" class="entry-date">2. Juni 2015</time></a></span>
								</div>
								<header class="entry-header">
									<h1 class="entry-title"><a rel="bookmark" href="#">Verjährung von Rechnungen - nach
											wie vielen Jahren?</a></h1>
								</header>
								<a class="post-thumbnail" aria-hidden="true" href="">
									<img src="images/post-img2.png" alt=""/>
								</a>
								<div class="entry-content">
									<p>Ein Sportwagen, eine Weltreise, ein Traumhaus an der Küste oder ganz
										bodenständig: die Altersvorsorge. Wir alle haben Ziele und Träume, für die es
										sich zu sparen lohnt <a href="#">Weiterlesen</a></p>
								</div>
							</article>
							<div class="artikel">
								<h3>Ältere Artikel</h3>
								<p>SEO Text here: Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
									nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
									voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
									gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor
									sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
									labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
									justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus
									est Lorem ipsum dolor sit amet.</p>
							</div>
						</div>
						<div class="news-r">
							<div class="right-img"><img src="images/news-right-img.jpg" alt=""/></div>
							<div class="right-img"><img src="images/news-right-img2.jpg" alt=""/></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_sidebar();
get_footer();
