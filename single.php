<?php
/**
 * The Template for displaying all single posts
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div class="page-title">
				<div class="center">
					<p class="single-heading"><span>Score Kompass Blog</span> — Ihr persönlicher Finanzratgeber</p>
				</div>
			</div>
			<div class="center">
				<div class="news-l">
					<article class="news-details">
						<div class="entry-meta">
							<span class="entry-date">
								<a rel="" href="#">
									<time datetime="" class="entry-date">
										<?php echo get_the_date( 'j. F Y', $post->ID ); ?>
									</time>
								</a>
							</span>
						</div>
						<header class="entry-header">
							<h1 class="entry-title">
								<a
										rel="bookmark"
										href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>">
									<?php the_title(); ?>
								</a>
							</h1>
						</header>
						<?php
						$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
						?>
						<a class="post-thumbnail" aria-hidden="true" href="">

						</a>
						<div class="entry-content">
							<?php
							while ( have_posts() ) :
								the_post();
								the_content();
							endwhile;
							?>
						</div>
					</article>
					<div class="weitere">
						<span class="articles-list-title">weitere artikel zum thema</span>
						<ul>
							<?php
							$args         = [
								'posts_per_page'   => 3,
								'offset'           => 0,
								'orderby'          => 'date',
								'order'            => 'DESC',
								'post_type'        => 'post',
								'post_status'      => 'publish',
								'suppress_filters' => true,
							];
							$relate_posts = get_posts( $args );
							foreach ( $relate_posts as $relate_post ) {
								$related_post_thumbnul = wp_get_attachment_url( get_post_thumbnail_id( $relate_post->ID ) );
								?>
								<li>
									<h2>
										<a href="<?php echo esc_url( get_permalink( $relate_post->ID ) ); ?>"><?php echo esc_html( $relate_post->post_title ); ?></a>
									</h2>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="news-r">
					<?php echo wp_kses_post( get_sidebar( 'blog' ) ); ?>
				</div>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();
